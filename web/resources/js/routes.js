app.config(function ($routeProvider, $locationProvider) {
    $routeProvider

        .when('/',
        {
            controller: 'MainController',
            templateUrl: 'partials/main.html'
        })

        .otherwise({redirectTo: '/404'});
});