var app = angular.module('app', ['ngCookies', 'ngRoute', 'readMore']);
app.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
    var original = $location.path;


    $location.pathNotReload = function (path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path])
    };

    $location.path = function (path) {
        return original.apply($location, [path])
    }
}]);
app.filter('unsafe', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };

});

app.filter('trusted', ['$sce', function ($sce) {
    return function (url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);


function template(tmpl, context, filter) {
    'use strict';
    return tmpl.replace(/\{([^\}]+)\}/g, function (m, key) {
// If key don't exists in the context we should keep template tag as is
        return key in context ? (filter ? filter(context[key]) : context[key]) : m;
    });
}

app.directive('match', function ($parse) {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            scope.$watch(function () {
                return $parse(attrs.match)(scope) === ctrl.$modelValue;
            }, function (currentValue) {
                ctrl.$setValidity('mismatch', currentValue);
            });
        }
    };
})
    .directive("ngFileSelect", ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                el.bind("change", function (e) {
                    scope.file = (e.srcElement || e.target).files[0];
                    scope.getFile();
                });
            }
        }
    }])

    .directive('ngSetMaxlength', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                attrs.$set("ngTrim", "false");
                var maxLength = parseInt(attrs.ngSetMaxlength, 10);
                ctrl.$parsers.push(function (value) {
                    if (value && value.length > maxLength) {
                        value = value.substr(0, maxLength);
                        ctrl.$setViewValue(value);
                        ctrl.$render();
                    }
                    return value;
                });
            }
        };
    })

    .directive('demoDatePicker', function () {
        return {
            restrict: 'EA',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                var datepicker;
                var optionsObj = {
                    onSelect: function (dateText, inst) {
                        scope.$apply(function () {
                            ngModel.$setViewValue(dateText);
                        });
                    }
                };
                datepicker = $(element).datepicker(optionsObj);
                scope.$watch(attrs.ngModel, function (value) {
                    if (value && value.indexOf("/") < 0) {
                        value = value.slice(0, 10);
                        var year = parseInt(value.substring(0, 4));
                        var month = parseInt(value.substring(5, 7));
                        var day = parseInt(value.substring(8, 10));
                        datepicker.datepicker('setDate', new Date(year, month - 1, day));
                    } else {
                        datepicker.datepicker('setDate', new Date(ngModel.$viewValue));
                    }
                    datepicker.datepicker('refresh');
                });
            }
        };
    })

    .directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == undefined) return '';
                    var transformedInput = inputValue.replace(/[^0-9+.]/g, '');
                    if (transformedInput.length > 2) {
                        transformedInput = transformedInput.substr(0, 2);
                    }
                    if (transformedInput != inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }
                    return transformedInput;
                });
            }
        }
    })

    .directive('showModeratorItem', function () {
        return {
            restrict: 'A',
            templateUrl: 'partials/blog/item.html'
        }
    })

    .directive('jdatepicker', function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, element, attrs, ctrl) {
                $(element).datepicker({
                    dateFormat: 'yy-mm-dd',
                    onSelect: function(date) {
                        ctrl.$setViewValue(date);
                        ctrl.$render();
                        scope.$apply();
                    }
                });
            }
        };
    })

    .directive('ngComments', function () {
        return {
            restrict: 'A',
            controller: 'ItemCommentController',
            templateUrl: 'partials/blog/comment_form.html'
        }
    });




app.config(function ($routeProvider, $locationProvider) {
    $routeProvider

        .when('/',
        {
            controller: 'MainController',
            templateUrl: 'partials/main.html'
        })
        .otherwise({redirectTo: '/404'});
});app.controller('MainController', [
    '$scope',
    '$location',
    function ($scope, $location) {

        (function ($) {
            $.fn.arctic_scroll = function (options) {

                var defaults = {
                    elem: $(this),
                    speed: 500
                };
                var options = $.extend(defaults, options);

                options.elem.click(function (event) {
                    event.preventDefault();
                    var offset = ($(this).attr('data-offset')) ? $(this).attr('data-offset') : false,
                        position = ($(this).attr('data-position')) ? $(this).attr('data-position') : false;
                    if (offset) {
                        var toMove = parseInt(offset);
                        $('html,body').stop(true, false).animate({scrollTop: ($(this.hash).offset().top + toMove)}, options.speed);
                    } else if (position) {
                        var toMove = parseInt(position);
                        $('html,body').stop(true, false).animate({scrollTop: toMove}, options.speed);
                    } else {
                        $('html,body').stop(true, false).animate({scrollTop: ($(this.hash).offset().top)}, options.speed);
                    }
                });

            };
        })(jQuery);

        $(function () {
            $(".scroller").arctic_scroll({
                speed: 600
            });
        });

        setTimeout(function() {
            $('.inner div').addClass('done');

            setTimeout(function() {
                $('.inner div').addClass('page');

                setTimeout(function() {
                    $('.pageLoad').addClass('off');

                    $('body, html').addClass('on');

                    setTimeout(function() {
                        $('.next').addClass('opac');
                    }, 250)
                }, 500)
            }, 500)
        }, 1500)
    }]);
