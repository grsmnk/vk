package com.qds.vkwrapper.gui;

import com.qds.vkwrapper.controllers.MainController;
import com.qds.vkwrapper.utils.Logger;
import com.qds.vkwrapper.utils.LoggerUtil;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Vladislav Gerasimenko on 17.12.15.
 */
public class MainPane extends Pane {
    private static final Logger LOGGER = LoggerUtil.getLogger(MainPane.class);

    public static final String PANE_SOURCE_NAME = "/client/Main.fxml";
    private MainController controller;

    public MainPane() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        URL resource = getClass().getResource(PANE_SOURCE_NAME);
        if (resource != null) {
            try {
                Pane panel = fxmlLoader.load(resource.openStream());
                controller = fxmlLoader.getController();
                getChildren().add(panel);
            } catch (IOException e) {
                LOGGER.printStackTrace(e);
            }
        } else {
            LOGGER.error("Cannot load pane %s", PANE_SOURCE_NAME);
        }
    }

    public MainController getController() {
        return controller;
    }
}
