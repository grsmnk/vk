package com.qds.vkwrapper.gui;

import com.qds.vkwrapper.core.Session;
import javafx.stage.Stage;

/**
 * Created by Vladislav Gerasimenko on 17.12.15.
 */
public class MainStage {

    private static Stage stage;

    private MainStage() {
    }

    public static Stage getStageInstance() {
        if (stage == null) {
            stage = new Stage();
            stage.setTitle("Working...");
            stage.setOnCloseRequest(event -> {
                stage = null;
            });
        }
        System.out.println(Session.getInstance().getAttribute("TOKEN"));
        return stage;
    }
}
