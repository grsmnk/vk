package com.qds.vkwrapper.gui;

import javafx.stage.Stage;

/**
 * Created by Vladislav Gerasimenko on 17.12.15.
 */

public class LoginStage {

    private static Stage stage;

    private LoginStage() {

    }

    public static Stage getStageInstance() {
        if (stage == null) {
            stage = new Stage();
            stage.setOnCloseRequest(event -> stage = null);
            stage.setResizable(false);
        }
        return stage;
    }

}
