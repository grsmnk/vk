package com.qds.vkwrapper.gui;

import com.qds.vkwrapper.controllers.VkLoginController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Vladislav Gerasimenko on 05.01.16.
 */
public class VkLoginPane extends Pane {

    public static final String PANE_SOURCE_NAME = "/client/VkLogin.fxml";
    private VkLoginController controller;

    public VkLoginPane() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        URL resource = getClass().getResource(PANE_SOURCE_NAME);
        if (resource != null) {
            try {
                Pane panel = fxmlLoader.load(resource.openStream());
                controller = fxmlLoader.getController();
                getChildren().add(panel);
            } catch (IOException e) {
//                LOGGER.printStackTrace(e);
            }
        } else {

//            LOGGER.error("Cannot load pane %s", PANE_SOURCE_NAME);
        }
    
    }

    public VkLoginController getController() {
        return controller;
    }
}
