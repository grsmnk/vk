package com.qds.vkwrapper.gui;

import javafx.stage.Stage;

/**
 * Created by Vladislav Gerasimenko on 05.01.16.
 */
public class VkLoginStage {
    private static Stage stage;

    private VkLoginStage() {

    }

    public static Stage getStageInstance() {
        if (stage == null) {
            stage = new Stage();
            stage.setOnCloseRequest(event -> stage = null);
            stage.setResizable(false);
        }
        return stage;
    }
}
