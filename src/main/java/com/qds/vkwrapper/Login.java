package com.qds.vkwrapper;

import com.qds.vkwrapper.controllers.LoginController;
import com.qds.vkwrapper.pojo.User;
import com.qds.vkwrapper.utils.Logger;
import com.qds.vkwrapper.utils.LoggerUtil;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Login extends Application {

    private String token;

    private static final Logger LOGGER = LoggerUtil.getLogger(Application.class);
    public static final String LOGIN_FXML = "/client/Login.fxml";

    public static User user;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Агитатор");
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(LOGIN_FXML));
            Parent root = loader.load();
            ((LoginController) loader.getController()).setStage(primaryStage);
            primaryStage.setScene(new Scene(root));
        } catch (IOException e) {
            LOGGER.printStackTrace(e);
        }
        primaryStage.show();
    }

}
