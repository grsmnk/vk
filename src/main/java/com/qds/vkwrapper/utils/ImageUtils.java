package com.qds.vkwrapper.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ImageUtils {

    public static void resize(String inputImagePath,
                              String outputImagePath, int widthArea, int heightArea) throws IOException {
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
        int originalWidth = inputImage.getWidth();
        int originalHeight = inputImage.getHeight();
        double dWidth = (double)widthArea / (double)originalWidth;
        double dHeight = (double)heightArea / (double)originalHeight;
        double delta;
        if(dWidth < dHeight) {
            delta = (((double)widthArea)/((double)originalWidth));
        } else {
            delta = (((double)heightArea)/((double)originalHeight));
        }
        int scaledWidth = (int) (inputImage.getWidth() * delta);
        int scaledHeight = (int) (inputImage.getHeight() * delta);
        BufferedImage outputImage = new BufferedImage(scaledWidth, scaledHeight, inputImage.getType());

        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }

    public static String encode (String value) {
        String encodedString = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(value.getBytes());
            byte[] bytes = md.digest();
            StringBuilder stringBuilder = new StringBuilder();
            for(int i = 0; i < bytes.length; i++) {
                stringBuilder.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            encodedString = stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return encodedString;
    }
}
