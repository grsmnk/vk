package com.qds.vkwrapper.utils;

import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

public class JsonUtils {

	private static ThreadLocal<String> tl = new ThreadLocal<String>(){
		public String initialValue() {
			return DatesUtils.DATE_FORMAT;
		}
	};

	private static String dateFormat = tl.get();

	private static final GsonBuilder gb = createGson();

	private static Logger LOGGER = LoggerUtil.getLogger(JsonUtils.class);

	public static String getJson(Object object) {
		return gb.create().toJson(object);
	}

	public static <T extends Object> T fromJson(Class clazz, String json) {
		return (T) gb.create().fromJson(json, clazz);
	}

	public static String setDefaultDateFormat () {
		dateFormat = DatesUtils.DATE_FORMAT;
		return dateFormat;
	}

	public static void setDateFormat (String df) {
		dateFormat = df;
	}

	private static GsonBuilder createGson() {
		return new GsonBuilder()
				.setPrettyPrinting()
				.serializeNulls()
				.setLongSerializationPolicy(LongSerializationPolicy.STRING)
				.excludeFieldsWithoutExposeAnnotation()
				.disableHtmlEscaping();
	}

}
