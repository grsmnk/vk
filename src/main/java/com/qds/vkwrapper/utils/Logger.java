package com.qds.vkwrapper.utils;

public class Logger {
    private org.slf4j.Logger logger = null;

    public Logger(org.slf4j.Logger logger) {
        this.logger = logger;
    }

    public void trace(String format, Object... args) {
        if(this.logger.isTraceEnabled() && this.logger != null) {
            this.logger.trace(String.format(format, args));
        }

    }

    public void debug(String format, Object... args) {
        if(this.logger.isDebugEnabled() && this.logger != null) {
            this.logger.debug(String.format(format, args));
        }

    }

    public void info(String format, Object... args) {
        if(this.logger.isInfoEnabled() && this.logger != null) {
            this.logger.info(String.format(format, args));
        }

    }

    public void warn(String format, Object... args) {
        if(this.logger.isWarnEnabled() && this.logger != null) {
            this.logger.warn(String.format(format, args));
        }

    }

    public void error(String format, Object... args) {
        if(this.logger.isErrorEnabled() && this.logger != null) {
            this.logger.error(String.format(format, args));
        }

    }

    public void fatal(String format, Object... args) {
        if(this.logger.isErrorEnabled() && this.logger != null) {
            this.logger.error(String.format(format, args));
        }

    }

    public void printStackTrace(Throwable e) {
        if(this.logger != null) {
            this.logger.error(String.format("Exception. Reason %s. Stacktrace %s.", new Object[]{e.getMessage(), e.getStackTrace()}));
        }

    }
}

