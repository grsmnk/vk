package com.qds.vkwrapper.utils;


import java.util.Calendar;
import java.util.Date;


public class DatesUtils {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
//    public static final String DATE_TIME_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ss.sssZ";
//    public static final String DATE_TIME_REGEX = "^(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2})$";

    public static boolean compareTwoDates(Date biggerDate, Date lesserDate) {
        return biggerDate.getTime() > lesserDate.getTime();
    }

    private static Calendar removeTimePartInternal(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar;
    }

    public static Date removeTimePart(Date date) {
        return removeTimePartInternal(date).getTime();
    }

    public static Date getNextMidnight(Date date) {
        Calendar calendar = removeTimePartInternal(date);
        calendar.add(5, 1);
        return calendar.getTime();
    }

    public static Date getLastNDate(Date date, int count) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(5, -count);
        return removeTimePart(calendar.getTime());
    }
}
