package com.qds.vkwrapper.utils;

import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class LoggerUtil {
    protected static Map<Class, Logger> loggerInstances = new HashMap();
    protected static Logger nullLogger = new Logger((org.slf4j.Logger)null);
    private static org.slf4j.Logger outLogger = LoggerFactory.getLogger("SystemOut");
    private static org.slf4j.Logger errLogger = LoggerFactory.getLogger("SystemErr");
    protected static boolean disable = false;

    public LoggerUtil() {
    }

    public static void initialize() {
    }

    private static PrintStream createLoggingProxy(final PrintStream realPrintStream, final org.slf4j.Logger logger, Level loggingLevel) {
        if(loggingLevel.equals(Level.INFO)) {
            return new PrintStream(realPrintStream) {
                public void print(String string) {
                    realPrintStream.print(string);
                    logger.info(string);
                }
            };
        } else if(loggingLevel.equals(Level.WARNING)) {
            return new PrintStream(realPrintStream) {
                public void print(String string) {
                    realPrintStream.print(string);
                    logger.warn(string);
                }
            };
        } else {
            getLogger(LoggerUtil.class).warn("Couldn\'t initialize proxy stream between logger %s and system out.", new Object[]{logger.getName()});
            return null;
        }
    }

    public static PrintStream getRedirectedToLoggerErrPrintStream(PrintStream platformSystemErrStream) {
        PrintStream errStream;
        if(null != (errStream = createLoggingProxy(platformSystemErrStream, errLogger, Level.WARNING))) {
            return errStream;
        } else {
            getLogger(LoggerUtil.class).warn("Couldn\'t redirect err stream %s to the logger.", new Object[]{platformSystemErrStream});
            return platformSystemErrStream;
        }
    }

    public static PrintStream getRedirectedToLoggerOutPrintStream(PrintStream platformSystemOutStream) {
        PrintStream outStream;
        if(null != (outStream = createLoggingProxy(platformSystemOutStream, outLogger, Level.INFO))) {
            return outStream;
        } else {
            getLogger(LoggerUtil.class).warn("Couldn\'t redirect out stream %s to the logger.", new Object[]{platformSystemOutStream});
            return platformSystemOutStream;
        }
    }

    public static Logger getLogger(Class clazz) {
        if(disable) {
            return nullLogger;
        } else {
            Logger logger = (Logger)loggerInstances.get(clazz);
            if(null == logger) {
                logger = new Logger(LoggerFactory.getLogger(clazz));
                loggerInstances.put(clazz, logger);
            }

            return logger;
        }
    }

    public static void disable() {
        disable = true;
    }

    static {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
    }
}
