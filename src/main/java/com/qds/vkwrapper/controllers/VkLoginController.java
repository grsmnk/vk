package com.qds.vkwrapper.controllers;

import com.qds.vkwrapper.core.ServerProxy;
import com.qds.vkwrapper.core.Session;
import com.qds.vkwrapper.gui.LoginStage;
import com.qds.vkwrapper.gui.MainPane;
import com.qds.vkwrapper.gui.MainStage;
import com.qds.vkwrapper.utils.Logger;
import com.qds.vkwrapper.utils.LoggerUtil;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Worker;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Vladislav Gerasimenko on 05.01.16.
 */
public class VkLoginController implements Initializable {

    private String token;
    private static final Logger LOGGER = LoggerUtil.getLogger(VkLoginController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Stage loginStage = LoginStage.getStageInstance();
        loginStage.setTitle("Авторизация в вк");
        final WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        webEngine.load(ServerProxy.VK_AUTHENTICATION_REQUEST);
        loginStage.setScene(new Scene(browser, ServerProxy.VK_AUTHENTICATION_WINDOW_WIDTH, ServerProxy.VK_AUTHENTICATION_WINDOW_HEIGHT));
        loginStage.show();

        webEngine.getLoadWorker().stateProperty().addListener((observableValue, oldState, state) -> {
            if (state.equals(Worker.State.SUCCEEDED)) {
                Timeline checkEveryThreeSeconds = new Timeline(new KeyFrame(Duration.seconds(3), event -> {
                    if (checkIfLogged(webEngine)) {
                        if (token == null) {
                            token = webEngine.getLocation().split("=")[1];
                            token = token.split("&")[0];
                            LOGGER.info("Token value is: %s", token);
                            Session.getInstance().setAttribute(ServerProxy.SESSION_ATTRIBUTE_TOKEN, token);
                            Stage mainStage = MainStage.getStageInstance();
                            mainStage.setScene(new Scene(new MainPane()));
                            loginStage.close();
                            mainStage.show();
                        }
                    }
                }));
                checkEveryThreeSeconds.setCycleCount(Timeline.INDEFINITE);
                checkEveryThreeSeconds.play();
            }
        });
    }

    public boolean checkIfLogged(WebEngine webEngine) {
        return webEngine.getLocation().split("=")[1].split("&")[0].length() > 20;
    }

}
