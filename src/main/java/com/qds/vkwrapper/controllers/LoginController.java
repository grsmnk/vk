package com.qds.vkwrapper.controllers;


import com.qds.vkwrapper.core.ServerProxy;
import com.qds.vkwrapper.core.Session;
import com.qds.vkwrapper.gui.VkLoginPane;
import com.qds.vkwrapper.pojo.User;
import com.qds.vkwrapper.utils.JsonUtils;
import com.qds.vkwrapper.utils.Logger;
import com.qds.vkwrapper.utils.LoggerUtil;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Vladisalv Gerasimenko on 29.09.15.
 */
public class LoginController implements Initializable {

    private static final Logger LOGGER = LoggerUtil.getLogger(LoginController.class);

    @FXML
    private TextField loginField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button enterButton;
    @FXML
    private Label errorLabel;

    private Stage primaryStage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        passwordField.setOnKeyPressed(eventEnter -> {
            if (eventEnter.getCode() == KeyCode.ENTER) {
                onEnterButtonClicked();
            }
        });
    }

    public void setStage(Stage stage) {
        this.primaryStage = stage;
    }

    @FXML
    private void onEnterButtonClicked() {
        User user = new User();
        user.setLogin(loginField.getText());
        user.setPassword(passwordField.getText());
        String urlParameters = JsonUtils.getJson(user);
        System.out.println(urlParameters);
        try {
            User result = ServerProxy.sendLoginPass(urlParameters);
            if (result != null && result.getLogin().equals("alex") && result.getPassword().equals("123456")) {
                Session.getInstance().setAttribute(ServerProxy.SESSION_ATTRIBUTE_USER, result);
                Scene scene = new Scene(new VkLoginPane());
                primaryStage.setScene(scene);
                primaryStage.close();
            } else {
                errorLabel.setText("логин или пароль не верны");
                errorLabel.setTextFill(Color.RED);
                passwordField.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.printStackTrace(e);
            errorLabel.setText("не уда’тся установить соедение с сервером");
            errorLabel.setTextFill(Color.RED);
        }
    }
}


