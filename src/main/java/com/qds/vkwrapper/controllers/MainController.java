package com.qds.vkwrapper.controllers;

import com.qds.vkwrapper.core.Session;
import com.qds.vkwrapper.core.VkRequestsSender;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * Created by Vladislav Gerasimenko on 17.12.15.
 */
public class MainController implements Initializable {

    public AnchorPane root;
    public TextField inputField;
    public Button submitButton;
    public Label label;
    private WebView browser;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        createGetMessageBrowser();
    }

    private void createGetMessageBrowser() {
        if (browser == null) {
            browser = new WebView();
            WebEngine webEngine = browser.getEngine();
//            webEngine.load(String.format(MESSAGES_GET, ChatPool.getToken()));
            Timeline checkEveryThreeSeconds = new Timeline(new KeyFrame(Duration.seconds(1), event -> {
//                if (this.currentChatTab != null) {
//                    checkNewMessage();
                System.out.println("le");
//                }
            }));
            checkEveryThreeSeconds.setCycleCount(Timeline.INDEFINITE);
            checkEveryThreeSeconds.play();
        }
    }

    public void getText(ActionEvent actionEvent) {

    }

    @FXML
    public void submit(Event genEvent) {
        if (genEvent.getEventType() == ActionEvent.ACTION || ((KeyEvent) genEvent).getCode() == KeyCode.ENTER) {
            genEvent.consume();
            try {
                sendRequestToVk();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void showDialogMessage(String text) {
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        Scene scene = new Scene(new Group(new Text(25, 25, text)));
        System.out.println(text);
        dialog.setScene(scene);
        dialog.show();
    }

    private void sendRequestToVk() throws IOException {
        System.out.println(inputField.getText());
        if (!inputField.getText().isEmpty()) {
            String vkMethod = inputField.getText();
            String responseFromVk = null;
            switch (vkMethod) {
                case "friends.get":
                    responseFromVk = VkRequestsSender.getFriendsOfUser("145113317");
                    showDialogMessage(responseFromVk);
                    break;
                case "messages.send":
                    Random rand = new Random();
                    int n = rand.nextInt(50) + 1;
                    String message = "привет" + n;
                    responseFromVk = VkRequestsSender.sendMessage(message, "6573534");
                    showDialogMessage(responseFromVk);
                    break;
                default:
                    showDialogMessage("ERROR");
                    break;
            }
//            String vkRequest = String.format("%s%s?%s&access_token=%s&version=5.3", VkRequestsSender.VK_API_METHOD, vkMethod, "user_id=145113317&fields=sex,city,bdate,site,contacts,connections,can_write_private_message", Session.getInstance().getAttribute("TOKEN"));
//            System.out.println(vkRequest);
//            URL url = new URL(vkRequest);
//            responseFromVk = VkRequestsSender.sendRequest(url, false, "GET");
//            System.out.println(responseFromVk);
//            showDialogMessage(responseFromVk.toString());
            inputField.clear();
        }
    }

    private String getTaskFromServer() {
        URL url = null;
        StringBuilder response;
        try {
            url = new URL("host+’api/v1/long_tasks");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (url != null) {
            response = VkRequestsSender.sendRequest(url, false, "POST");
        } else {
            return null;
        }
        if (response != null) {
            return response.toString();
        }
        return null;
    }

}
