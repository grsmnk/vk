package com.qds.vkwrapper.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServerLongTaskResponse implements Serializable {

    public static final String DATA_JSON = "data";

    @Expose
    @SerializedName(DATA_JSON)
    private ServerLongTaskData serverLongTaskData;

    public ServerLongTaskData getServerLongTaskData () {
        return serverLongTaskData;
    }

    public void setServerLongTaskData (ServerLongTaskData serverLongTaskData) {
        this.serverLongTaskData = serverLongTaskData;
    }
}
