package com.qds.vkwrapper.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServerLongTaskData implements Serializable {

    public static final String METHOD_JSON = "method";
    public static final String TYPE_JSON = "type";
    public static final String PARAMS_JSON = "params";

    @Expose
    @SerializedName(METHOD_JSON)
    private String method;

    @Expose
    @SerializedName(TYPE_JSON)
    private String type;

    @Expose
    @SerializedName(PARAMS_JSON)
    private ServerLongTaskParams serverLongTaskParams;

    public String getMethod () {
        return method;
    }

    public void setMethod (String method) {
        this.method = method;
    }

    public String getType () {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public ServerLongTaskParams getServerLongTaskParams () {
        return serverLongTaskParams;
    }

    public void setServerLongTaskParams (ServerLongTaskParams serverLongTaskParams) {
        this.serverLongTaskParams = serverLongTaskParams;
    }
}
