package com.qds.vkwrapper.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vladislav Gerasimenko on 29.12.15.
 */
public class Params implements Serializable {

    public static final String ACCESS_TOKEN = "access_token";
    @Expose
    @SerializedName(ACCESS_TOKEN)
    private String access_token;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
