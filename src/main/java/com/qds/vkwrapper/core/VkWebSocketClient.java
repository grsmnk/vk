package com.qds.vkwrapper.core;


import javax.websocket.*;
import java.net.URI;

@ClientEndpoint
public class VkWebSocketClient {
    javax.websocket.Session userSession = null;
    private MessageHandler messageHandler;

    public VkWebSocketClient(URI endpointURI) {
        try {
            WebSocketContainer container = ContainerProvider
                    .getWebSocketContainer();
            container.connectToServer(this, endpointURI);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @OnOpen
    public void onOpen(javax.websocket.Session userSession) {
        this.userSession = userSession;
    }

    @OnClose
    public void onClose(com.qds.vkwrapper.core.Session userSession, CloseReason reason) {
        this.userSession = null;
    }

    @OnMessage
    public void onMessage(String message) {
        if (this.messageHandler != null)
            this.messageHandler.handleMessage(message);
    }

    public void addMessageHandler(MessageHandler msgHandler) {
        this.messageHandler = msgHandler;
    }

    public void sendMessage(String message) {
        this.userSession.getAsyncRemote().sendText(message);
    }

    public static interface MessageHandler {
        public void handleMessage(String message);
    }
}
