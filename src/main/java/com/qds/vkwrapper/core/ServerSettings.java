package com.qds.vkwrapper.core;

/**
 * Created by Vladislav Gerasimenko on 05.01.16.
 */
public class ServerSettings {
    public static final String HOST = "host";
    public static final String PORT = "port";
}
