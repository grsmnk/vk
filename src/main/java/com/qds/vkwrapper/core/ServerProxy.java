package com.qds.vkwrapper.core;


import com.qds.vkwrapper.pojo.User;
import com.qds.vkwrapper.utils.JsonUtils;
import com.qds.vkwrapper.utils.Logger;
import com.qds.vkwrapper.utils.LoggerUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerProxy {

    private static final Logger LOGGER = LoggerUtil.getLogger(ServerProxy.class);
    private static final String ENCODING_NAME = "UTF-8";
    private static User currentUser;
    private static final String ERROR = "error";
    public static final String POST_REQUEST = "POST";
    public static final String GET_REQUEST = "GET";
    public static final String SESSION_ATTRIBUTE_USER = "USER";
    public static final String SESSION_ATTRIBUTE_TOKEN = "TOKEN";
    public static final String VK_AUTHENTICATION_REQUEST = "http://oauth.vk.com/authorize?" +
            "client_id=5191726&" +
            "scope=528384&" +
            "redirect_uri=https://oauth.vk.com/blank.html&" +
            "display=page&" +
            "v=5.34&" +
            "response_type=token";
    public static final int VK_AUTHENTICATION_WINDOW_WIDTH = 658;
    public static final int VK_AUTHENTICATION_WINDOW_HEIGHT = 378;

    public static StringBuilder getResponse(String url, String urlParameters, String requestMethodName) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod(requestMethodName);
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept-Charset", ENCODING_NAME);
        con.setDoOutput(true);
        if (POST_REQUEST.equals(requestMethodName)) {
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), ENCODING_NAME));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        int responseCode = con.getResponseCode();
        LOGGER.debug(con.getResponseMessage());
        LOGGER.debug("Sending '%s' request to URL : %s", requestMethodName, url);
        LOGGER.debug("Post parameters : %s", urlParameters);
        LOGGER.debug("Response Code : %s", responseCode);
        return response;
    }

    public static User sendLoginPass(String urlParameters) throws Exception {
        String url = String.format("http://%s:%s/api/login", ServerSettings.HOST, ServerSettings.PORT);
//        StringBuilder response = getResponse(url, urlParameters, POST_REQUEST);
//        if (ERROR.equalsIgnoreCase(response.toString())) {
//            return null;

//        currentUser = JsonUtils.fromJson(User.class, response.toString());
////        LOGGER.info("Set current user: %s %s", currentUser.getName(), currentUser.getSurname());
//        Session.getInstance().setAttribute("USER", currentUser);

        currentUser = JsonUtils.fromJson(User.class, urlParameters);
        return currentUser;
    }

    public static User getUser() {
        return currentUser;
    }

}
