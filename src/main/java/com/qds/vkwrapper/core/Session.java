package com.qds.vkwrapper.core;

import java.util.HashMap;

/**
 * Created by Vladislav Gerasimenko on 17.12.15.
 */
public class Session {
    private HashMap<String, Object> data = new HashMap<>();
    private static Session instance = new Session();

    public void setAttribute(String name, Object value) {
        data.put(name, value);
    }

    public Object getAttribute(String name) {
        return data.get(name);
    }

    public static Session getInstance() {
        return instance;
    }
}
