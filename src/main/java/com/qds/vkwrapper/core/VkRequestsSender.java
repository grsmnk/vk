package com.qds.vkwrapper.core;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Vladislav Gerasimenko on 30.12.15.
 */
public class VkRequestsSender {

    public static final String VK_URL_PARAMETERS = "user_id=%s&message=%s&access_token=%s";
    private static final String MESSAGES_SEND_REQUEST = "https://api.vk.com/method/messages.send";
    public static final String VK_API_METHOD = "https://api.vk.com/method/";


    public static String getFriendsOfUser(String vkId) throws IOException {

        String vkRequest = String.format("https://api.vk.com/method/friends.get" +
                "?user_id=%s&fields=sex,city,bdate,site,contacts,connections,can_write_private_message&version=5.3", vkId);
        URL obj = new URL(vkRequest);
        StringBuilder response = null;
        while (response == null) {
            response = sendRequest(obj, true, "GET");
        }
        return response.toString();
    }


    public static String sendMessage(String message, String toVkId) {
        String textForVK = null;
        String response = "";
        try {
            textForVK = new String(message.getBytes("UTF-8"));
            textForVK = textForVK.replaceAll("#", "%23").replaceAll("&", "%26").replaceAll("\\+", "%2B");
            String urlParameters = String.format(VK_URL_PARAMETERS, toVkId, textForVK, Session.getInstance().getAttribute("TOKEN"));
            URL url;
            url = new URL(MESSAGES_SEND_REQUEST);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(urlParameters);
            writer.flush();
            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                response = line;
            }
            System.out.println(response);
            writer.close();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static StringBuilder sendRequest(URL url, boolean showUrl, String requestMethod) {
        if (showUrl) {
            System.out.println(String.format("Request string: %s", url.toString()));
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod(requestMethod);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept-Charset", "UTF-8");
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder response = null;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"))) {
            String inputLine;
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

}

